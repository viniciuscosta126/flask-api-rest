from flask import make_response, request
from flask_restful import Resource
from api import api
from ..schemas import funcionario_schema
from flask import request, jsonify, make_response
from ..entidades import funcionario
from ..services import funcionario_service


class FuncionarioList(Resource):
    def get(self):
        funcionarios = funcionario_service.listar_funcionarios()
        fc = funcionario_schema.FuncionarioSchema(many=True)
        return make_response(fc.jsonify(funcionarios), 200)

    def post(self):
        fc = funcionario_schema.FuncionarioSchema()
        validate = fc.validate(request.json)
        if validate:
            return make_response(jsonify(validate), 404)
        else:
            nome = request.json["nome"]
            idade = request.json["idade"]
            funcionario_novo = funcionario.Funcionario(nome=nome, idade=idade)
            result = funcionario_service.cadastrar_funcionario(funcionario_novo)
            return make_response(fc.jsonify(result), 201)


class FuncionarioDetail(Resource):
    def get(self, id):
        funcionario = funcionario_service.listar_funcionarios_id(id)
        if funcionario is None:
            return make_response("Funcionario nao encontrado", 404)
        fc = funcionario_schema.FuncionarioSchema()
        return make_response(fc.jsonify(funcionario), 200)

    def put(self, id):
        funcionario_bd = funcionario_service.listar_funcionarios_id(id)
        if funcionario_bd is None:
            return make_response("Funcionario nao encontrado", 404)
        fc = funcionario_schema.FuncionarioSchema()
        validate = fc.validate(request.json)
        if validate:
            return make_response(jsonify(validate), 404)
        else:
            nome = request.json["nome"]
            idade = request.json["idade"]
            funcionario_novo = funcionario.Funcionario(nome=nome, idade=idade)
            funcionario_service.editar_funcionario(funcionario_bd, funcionario_novo)
            funcionario_atualizado = funcionario_service.listar_funcionarios_id(id)
            return make_response(fc.jsonify(funcionario_atualizado), 201)

    def delete(self, id):
        funcionario = funcionario_service.listar_funcionarios_id(id)
        if funcionario is None:
            return make_response(jsonify("Funcionario nao encontrado"), 404)
        funcionario_service.delete_funcionar(funcionario)
        return make_response('', 204)


api.add_resource(FuncionarioList, "/funcionarios")
api.add_resource(FuncionarioDetail, "/funcionarios/<int:id>")
