from api import ma
from marshmallow import fields
from ..models import projeto_model



class ProjetoSchema(ma.Schema):
    class Meta:
        model = projeto_model.Projeto
        fields = ("id", "nome", "descricao","tarefas","funcionarios")

    nome = fields.String(required=True)
    descricao = fields.String(required=True)
    tarefas = fields.List(fields.String)
    funcionarios = fields.List(fields.String)