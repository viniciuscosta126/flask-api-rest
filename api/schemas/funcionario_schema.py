from api import ma
from marshmallow import fields
from ..models import funcionario_model


class FuncionarioSchema(ma.Schema):
    class Meta:
        model = funcionario_model.Funcionario
        fields = ("id","nome","idade","projetos")

    nome = fields.String(required = True)
    idade = fields.Integer(required= True)
    projetos = fields.List(fields.String)